package transit.littlecity.messaging

import com.google.flatbuffers.FlatBufferBuilder
import spock.lang.Specification

import java.time.LocalTime

class StopArrivalDepartureUpdateTests extends Specification {
    FlatBufferBuilder flatBufferBuilder

    def setup () {
        flatBufferBuilder = new FlatBufferBuilder(1);
    }

    def "can create stop routes message"() {
        given:
        int rad1 = createRouteArrivalDeparture("Saskatoon Transit_33305",
                                               "MEADOWGREEN",
                                               LocalTime.now().minusMinutes(21),
                                               LocalTime.now().minusMinutes(9))
        flatBufferBuilder.finish(rad1)
        int rad2 = createRouteArrivalDeparture("Saskatoon Transit_44305",
                                               "Silver Spring",
                                               LocalTime.now().minusMinutes(6),
                                               LocalTime.now().minusMinutes(24))
        flatBufferBuilder.finish(rad2)
        int stm = StopRoutesMessage.createStopRoutesMessage(
                flatBufferBuilder,
                flatBufferBuilder.createString("Saskatoon Transit_232"),
                flatBufferBuilder.createVectorOfTables([rad1, rad2] as int[]))
        flatBufferBuilder.finish(stm)

        when:
        def stopRouteMessage = StopRoutesMessage.getRootAsStopRoutesMessage(flatBufferBuilder.dataBuffer())

        then:
        stopRouteMessage.stopId() == 'Saskatoon Transit_232'
        stopRouteMessage.routesArrivalsAndDeparturesLength() == 2
    }

    private int createRouteArrivalDeparture(String routeId, String routeVehicle, LocalTime lastSeen, LocalTime nextArrival) {
        return RouteArrivalDeparture.createRouteArrivalDeparture(
                flatBufferBuilder,
                flatBufferBuilder.createString(routeId),
                flatBufferBuilder.createString(routeVehicle),
                lastSeen.toNanoOfDay(),
                nextArrival.toNanoOfDay()
        )
    }
}
