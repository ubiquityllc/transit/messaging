package transit.littlecity.messaging

import com.google.flatbuffers.FlatBufferBuilder
import spock.lang.Specification
import spock.lang.Title

import java.time.LocalTime

@Title("Route Arrival-Departure Message Tests")
class RouteArrivalDepartureUpdateTests extends Specification {

    FlatBufferBuilder flatBufferBuilder

    def setup () {
        flatBufferBuilder = new FlatBufferBuilder(1);
    }

    def "can create route arrival-departure message" () {
        given:
        int rad = RouteArrivalDeparture.createRouteArrivalDeparture(
                flatBufferBuilder,
                flatBufferBuilder.createString("Saskatoon Transit_3305"),
                flatBufferBuilder.createString("Lawson Heights"),
                LocalTime.now().minusMinutes(21).toNanoOfDay(),
                LocalTime.now().minusMinutes(9).toNanoOfDay())

        flatBufferBuilder.finish(rad)

        when:
        def arrivalDeparture = RouteArrivalDeparture.getRootAsRouteArrivalDeparture(flatBufferBuilder.dataBuffer());

        then:
        arrivalDeparture.routeId() == "Saskatoon Transit_3305"
        arrivalDeparture.routeVehicle() == "Lawson Heights"
        arrivalDeparture.lastSeen() != 0L
        arrivalDeparture.nextArrival() != 0L
    }
}
