package transit.littlecity.messaging

import com.google.flatbuffers.FlatBufferBuilder
import spock.lang.Specification

class StopInformationTests extends Specification {
    FlatBufferBuilder flatBufferBuilder

    def setup () {
        flatBufferBuilder = new FlatBufferBuilder(1);
    }

    def "can create stop information message"() {
        given:
        int geoLoc = GeoLocation.createGeoLocation(flatBufferBuilder, -106.6578f, 52.2931f)
        int stopInfo = StopInformation.createStopInformation(flatBufferBuilder,
                                                             flatBufferBuilder.createString("#ID"),
                                                             flatBufferBuilder.createString("Test Stop"),
                                                             flatBufferBuilder.createString("America/Regina"),
                                                             geoLoc,
                                                             StopInformation.createRouteInformationListVector(flatBufferBuilder, [] as int[]))

        flatBufferBuilder.finish(stopInfo)
        when:
        StopInformation stopInformation = StopInformation.getRootAsStopInformation(flatBufferBuilder.dataBuffer())

        then:
        stopInformation.stopId() == '#ID'
        stopInformation.stopName() == 'Test Stop'
        stopInformation.stopTimezone() == 'America/Regina'
        with(stopInformation.stopLocation()) {
            it.latitude() == 52.2931f
            it.longitude() == -106.6578f
        }
        stopInformation.routeInformationListLength() == 0
    }
}
