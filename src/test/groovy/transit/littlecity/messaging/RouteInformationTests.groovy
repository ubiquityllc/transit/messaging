package transit.littlecity.messaging

import com.google.flatbuffers.FlatBufferBuilder
import spock.lang.Specification

import java.awt.Color

class RouteInformationTests extends Specification {
    FlatBufferBuilder flatBufferBuilder

    def setup () {
        flatBufferBuilder = new FlatBufferBuilder(1);
    }

    def "can create route information message"() {
        given:
        int routeInfo = RouteInformation.createRouteInformation(flatBufferBuilder,
                                                                flatBufferBuilder.createString(UUID.randomUUID().toString()),
                                                                flatBufferBuilder.createString("Broadway"),
                                                                flatBufferBuilder.createString("90"),
                                                                flatBufferBuilder.createString("#0085F0"))
        flatBufferBuilder.finish(routeInfo)
        when:
        RouteInformation routeInformation = RouteInformation.getRootAsRouteInformation(flatBufferBuilder.dataBuffer())

        then:
        !routeInformation.routeId().empty
        routeInformation.routeLongName() == "Broadway"
        routeInformation.routeShortName() == '90'
        routeInformation.routeTextColor().toLowerCase() == Color.decode('#0085F0').with {
            String.format('#%02x%02x%02x', it.red, it.green, it.blue)
        }
    }
}
