module transit.littlecity.messaging {
    requires java.annotation;
    requires flatbuffers.java;
    exports transit.littlecity.messaging; // Generated files are in this package
}
